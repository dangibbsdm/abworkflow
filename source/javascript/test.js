var config = {
  slug: 'your-test-name-here',
  version: 'challenger',
};
TestRequirementCheck = setInterval( function() {

  if ( document.body && false == document.body.classList.contains(config.slug) ) {

    window.ab = {

      init: function() {

        // Add body class of test name & current challenger version
        document.body.className += " " + config.slug + " " + config.slug + '--' + config.version;

        // Add stylesheet
        var css = '%CSS%',
            head = document.head || document.getElementsByTagName('head')[0],
            style = document.createElement('style');

        style.type = 'text/css';

        if (style.styleSheet) {
          style.styleSheet.cssText = css;
        } else {
          style.appendChild(document.createTextNode(css));
        }

        head.appendChild(style);

        console.log('TEST RUNNING | ' + config.slug.toUpperCase() + ' - BN(%BN%) - ' + config.version);

      },

      challenger: function() {



      }

    };

    // Begin test
    ab.init(config);

    // Call challenger
    ab[config.version]()

    // Clear interval
    clearInterval(TestRequirementCheck);

  }

}, 1);