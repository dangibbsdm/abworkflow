// grab our gulp packages
var gulp  = require('gulp'),
    gutil = require('gulp-util'),
    jshint = require('gulp-jshint'),
    sourcemaps = require('gulp-sourcemaps'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    replace = require('gulp-replace'),
    fs = require('fs'),
    postcss      = require('gulp-postcss'),
    cssnext      = require('postcss-cssnext'),
    lost         = require('lost'),
    clipboard = require("gulp-clipboard"),
    cssnano = require('gulp-cssnano');

// define the default task and add the watch task to it
gulp.task('default', ['watch']);

// configure the jshint task
gulp.task('jshint', function() {
  return gulp.src('source/javascript/**/*.js')
    .pipe(jshint())
    .pipe(jshint.reporter('jshint-stylish'));
});

gulp.task('build-css', function () {
  return gulp.src('source/css/test.css')
    .pipe( postcss([
      require('precss'),
      lost(), //http://lostgrid.org/
      cssnext({browsers: ['last 2 version']}), // use css4 features
    ]) )
    .pipe(cssnano({zindex: false})) // compresses code
    .pipe( sourcemaps.write('.') )
    .pipe( gulp.dest('public/assets/stylesheets') );
});

//Configure javascript build
gulp.task('build-js', ['build-css'], function() {
  return gulp.src('source/javascript/**/*.js')
    //find and replace %CSS% and insert test.css
    .pipe(replace(/%CSS%/, function(s) {
        var style = fs.readFileSync('public/assets/stylesheets/test.css', 'utf8');
        return style;
    }))
    .pipe(replace(/%BN%/, function(s) {
        return Date.now();
    }))
    .pipe(sourcemaps.init())
      .pipe(concat('bundle.js'))
      .pipe(uglify({
        keep_fnames: true,
      }))
      .pipe(clipboard())
    .pipe(gulp.dest('public/assets/javascript'));
});

// configure which files to watch and what tasks to use on file changes
gulp.task('watch', function() {
  gulp.watch('source/javascript/**/*.js', ['jshint', 'build-js']);
  gulp.watch('source/css/**/*.css', ['build-js']);
});