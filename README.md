# AB Workflow #

### What is this repository for? ###

This is for making A/B tests easier to develop.

### How do I get set up? ###

Open Terminal & cd into directory
type 'npm install'
Once all packages are installed, type gulp & hit enter

When you save css & javascript after running gulp the scripts will be compiled into one file & copied to your clipboard.